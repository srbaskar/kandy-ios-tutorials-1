### Connection and Registration State

To receive the current connection state to the server, use the following code:

```objectivec
[[Kandy sharedInstance].access.connectionState
[[Kandy sharedInstance].access.registrationState
```

Registration state describes current signup status of the user.

Connection state describes current log-in status of the user.