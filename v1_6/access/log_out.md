### Log Out

This method logs the subscriber out of the Kandy server.

```objectivec
-(void)logout{
  [[Kandy sharedInstance].access logoutWithResponseCallback:^(NSError *error) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```
