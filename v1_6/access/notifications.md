### Notifications

You can be notified when the user's connection to the system is changed.

Implement the KandyAccessNotificationDelegate in the suitable class.

Then use the following code:

```objectivec
-(void)registerForAccessEvents{
  [[Kandy sharedInstance].access registerNotifications:self];
}
```


Once registered to Access notification, you should implement the following:

```objectivec
-(void) connectionStatusChanged:(EKandyConnectionState)connectionStatus{
  // Handle connection change
}

-(void) gotInvalidUser:(NSError*)error{
  // Invalid user
}

-(void) sessionExpired:(NSError*)error{
  // Session expired, you can renew it by calling to following method
  [[Kandy sharedInstance].access renewExpiredSession:^(NSError *error) {
  }];
}

-(void) SDKNotSupported:(NSError*)error{
  // Failure
}
```
