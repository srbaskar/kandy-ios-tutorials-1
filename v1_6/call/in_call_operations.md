### In-Call Operations

Kandy SDK provides you the option to perform several operations over the call: mute, switch camera, hold,...

In order to do so, use the call object relevant methods.

```objectivec
-(void) holdCall {
  id<KandyIncomingCallProtocol> call = <Call object>;
  [call holdWithResponseCallback:^(NSError *error) {
    if (error) {
      //Failure
    }
    else {
      //Success
    }
  }];
}
```
