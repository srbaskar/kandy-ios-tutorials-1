### Make a VoIP to PSTN Call

The following method initiates a VoIP to PSTN call, where:

* International phone number represents the destination phone number

_**Note:** The destination phone number must be an E164 formatted number without the '+' prefix, e.g: for US number (213) 456-7890 use 12134567890._

```objectivec
-(void)makePSTNCall {
  id <KandyOutgoingCallProtocol> outgoingPSTNCall = [[Kandy sharedInstance].services.call createPSTNCall: :@"caller international phone number" destination:@"ecallee international phone number" options:EKandyOutgingPSTNCallOptions_none];
  [outgoingPSTNCall establishWithResponseBlock:^(NSError *error) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```
