### Downloading Media Thumbnails

To download media thumbnails, use the following:

```objectivec
-(void)downloadMediaThumbnail:( id<KandyMessageProtocol>)kandyMessage {
  [[Kandy sharedInstance].services.chat downloadMediaThumbnail:kandyMessage thumbnailSize:EKandyThumbnailSize_medium progressCallback:^(KandyTransferProgress *transferProgress) {
    //Progress
  } responseCallback:^(NSError *error, NSString *fileAbsolutePath) {
    if (error) {
      //Failure
    } else {
      //Success
    }
  }];
}
```
