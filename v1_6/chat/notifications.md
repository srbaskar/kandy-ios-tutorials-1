### Notifications

You can receive Chat-related notifications (onChatReceived, onChatDelivered, onAutoDownloadProgress, onAutoDownloadFinished).

In order to receive Chat-related notifications, implement the KandyChatServiceNotificationDelegate in the suitable class.

You should acknowledge the received chat event unless, you wish to receive it again.

Then use the following code:

```objectivec
-(void)registerForChatEvents {
  [[Kandy sharedInstance].services.chat registerNotifications:self];
}
```

To handle Chat events, use the following formats:

```objectivec
-(void)onMessageReceived:(id<KandyMessageProtocol>)kandyMessage recipientType:(EKandyRecordType)recipientType{
  switch (kandyMessage.mediaItem.mediaType) {
    case EKandyFileType_text:
      //Your code here
      break;
    case EKandyFileType_image:
      //Your code here
      break;
    case EKandyFileType_video:
      //Your code here
      break;
    case EKandyFileType_audio:
      //Your code here
      break;
    case EKandyFileType_location:
      //Your code here
      break;
    case EKandyFileType_contact:
      //Your code here
      break;
    case EKandyFileType_file:
      //Your code here
      break;
    case EKandyFileType_custom:
      //Your code here
      break;
    default:
      break;
  }
}
```

* For Chat delivered:

```objectivec
-(void)onMessageDelivered:(KandyDeliveryAck *)ackData {
  //Your code here
}
```

* Auto download progress:

```objectivec
-(void) onAutoDownloadProgress:(KandyTransferProgress*)transferProgress kandyMessage:(id<KandyMessageProtocol>)kandyMessage {
  //Progress
}
```

* Auto download completion:

```objectivec
-(void) onAutoDownloadFinished:(NSError*)error fileAbsolutePath:(NSString*)path kandyMessage:(id<KandyMessageProtocol>)kandyMessage {
  if(error){
    //Failure
  } else{
    //Success
  }
}
```
