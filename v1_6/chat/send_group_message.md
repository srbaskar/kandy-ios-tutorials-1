### Send Group Message

```objectivec
-(void)sendGroupMsg{
  // if you want to create a group record
  // KandyRecord * kandyRecord = [[KandyRecord alloc] initWithUserName:@"groupUserName" domain:@"domain" type:EKandyRecordType_group];
  // if you already have a groupID
  KandyRecord * kandyRecord = self.kandyGroup.groupId;
  KandyChatMessage *textMessage = [[KandyChatMessage alloc] initWithText:@”Message text” recipient:kandyRecord];
  [[Kandy sharedInstance].services.chat sendChat:chatMessage progressCallback:^(KandyTransferProgress *transferProgress){
    //progress
  }
  responseCallback:^(NSError *error) {
    if (error){
      //Failure
    } else {
      //Success
    }
  }];
}
```
