### Settings

The following are the Chat settings:

* autoDownload\_media\_connectionType – when to allow the application to download the received media. (3G only, Wi-Fi only, Always, Never)
* autoDownload\_media\_maxSizeKB – Maximum size of downloaded media
* autoDownload\_thumbnail – whether the thumbnail should be downloaded automatically
* autoDownload\_thumbnailSize – the size of the auto downloaded thumbnail
* downloadPathBuilder – a protocol that you can conform to and override.
  The path builder allows you to set the download path for newly downloaded files and the file names according to your needs.
* mediaMaxUploadSizeInKB – the maximum size allowed for upload

To get and set Kandy Chat settings, use:

```objectivec
[[Kandy sharedInstance].services.chat.settings
```