### SMS Out

To send an SMS to a regular cellular phone use the following code (the SMS will be sent via Kandy service):

```objectivec
-(void)sendSMS: {
KandySMSMessage *smsMessage = [[KandySMSMessage alloc] initWithText:@”The SMS text” recipient:@”International number” displayName:@”SMS display name”];
[[Kandy sharedInstance].services.chat sendSMS:smsMessage responseCallback:^(NSError *error) {
  if (error)
  {
    //Failure
  }
  else
  {
    //Success
  }
  }];
}
```
