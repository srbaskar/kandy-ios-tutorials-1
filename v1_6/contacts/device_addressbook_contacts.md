### Device AddressBook Contacts

The following method returns the device contacts as id\<KandyContactProtocol\> objects. If operation failed, returns an error.

```objectivec
-(void)getDeviceContacts{
  [[Kandy sharedInstance].services.contacts getDeviceContactsWithResponseCallback:^(NSError *error, NSArray *kandyContacts) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```
