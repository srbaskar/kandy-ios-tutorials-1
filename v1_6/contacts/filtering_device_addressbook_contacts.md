### Filtering Device AddressBook Contacts

The following method returns the filtered device contacts as id\<KandyContactProtocol\> objects. If operation fails, it returns an error.

You can filter contacts in two ways:

* According to the fields the contacts contains: phones/emails/all (bitwise operation)
* By the display name, with a given "search" string. If filter is empty or nil, does not filter by text.

Filtering of the fields is prefix based.

```objectivec
-(void)filterDeviceContacts{
  [[Kandy sharedInstance].services.contacts getDeviceContactsWithFilter:EKandyDeviceContactFilter_phone | EKandyDeviceContactFilter_email textSearch:@"John" responseCallback:^(NSError *error, NSArray *kandyContacts) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```
