### Filtering Domain Directory Contacts

The following method returns the filtered domain directory contacts as id\<KandyContactProtocol\> objects. If the operation fails, it returns an error.

Filtering is done by a given text string, a EKandyDomainContactFilter enum indicating which fields to filter and a boolean specifying whether the filtering is case sensitive. Filtering of the fields logic is achieved by using "contains".

```objectivec
-(void)filterDomainDirectoryContacts {
  [[Kandy sharedInstance].services.contacts getFilteredDomainDirectoryContactsWithTextSearch:@"John" filterType:EKandyDomainContactFilter_firstAndLastName caseSensitive:NO responseCallback:^(NSError *error, NSArray *kandyContacts) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```
