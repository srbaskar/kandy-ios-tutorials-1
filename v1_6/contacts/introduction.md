## Contacts

The Contacts service enables you to access the device address book and the domain directory, retrieve and filter contacts and get notification on changes to the device address book.