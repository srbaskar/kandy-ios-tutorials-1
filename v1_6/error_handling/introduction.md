## Error Handling

Each error the SDK returns contains two properties:

1. Domain
2. Code

In each scenario, the domain will probably be KandyGeneralServiceErrorDomain or a domain that is relevant for your operation (for example: KandyCallServiceErrorDomain).

In each domain, there are multiple error codes, that represent the error scenarios the SDK recognized.

You should handle errors by the domain category, and by switching its relevant code using the following code:

```objectivec
-(void)errorHandling {
  [[Kandy sharedInstance].provisioning signoutWithResponseCallback:^(NSError *error) {
    BOOL isHandledError = YES;
    if ([error.domain isEqualToString:KandyGeneralServiceErrorDomain]) {
      switch (error.code) {
        case EKandyGeneralServiceError_NetworkError:
          // Error handling code
          break;
        case EKandyGeneralServiceError_RequestTimeout:
          // Error handling code
          break;
        default:
          isHandledError = NO;
          break;
      }
    }
    else if ([error.domain isEqualToString:KandySignupServiceErrorDomain]) {
      switch (error.code) {
        case EKandySignupErrors_userValidationFailed:
          // Error handling code
        case EKandySignupErrors_validationCodeExpired:
          // Error handling code
          // .
          // .
          // .
        default:
          isHandledError = NO;
          break;
      }
    }
    else {
      isHandledError = NO;
    }
  }];
}
```
