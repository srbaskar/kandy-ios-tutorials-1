### Add/Remove Group Participants

You can add or remove participants from your group.

```objectivec
-(void)addParticipant{
  KandyRecord * participant = [[KandyRecord alloc] initWithURI: @"participantURI”];
  KandyRecord * groupId = self.kandyGroup.groupId;
  [[Kandy sharedInstance].services.group addGroupParticipants:@[participant] groupId:groupId responseCallback:^(NSError *error, KandyGroup *group) {
    if (error) {
      //error
    } else {
      //participants added
     }
   }];
}
```
