### Group Create

KandyGroup can be created with name (optional) and image (optional).

```objectivec
-(void)createGroup{
  KandyGroupParams * kandyGroupParams = [[KandyGroupParams alloc] init];
  kandyGroupParams.groupName = @"groupName";
  kandyGroupParams.groupAbsoluteImagePath = @"pathToGroupImage"; //will also initialize the group.image
  [[Kandy sharedInstance].services.group createGroup:kandyGroupParams progressCallback:^(KandyTransferProgress *transferProgress) {
      NSLog(@"Group Image upload progress : %ld", (long)transferProgress.transferProgressPercentage);
  } responseCallback:^(NSError *error, KandyGroup *group) {
    if (error) {
      //error
    } else {
      //group created successfully
    }
  }];
}
```
