### Group Settings

The following are the Group settings:

* downloadPathBuilder – a protocol that you can conform to and override.<br />The path builder allows you to set the download path for newly downloaded files and the file names according to your needs.
* groupImageMaxUploadSizeInKB – the maximum size allowed for upload

To get and set Kandy Group settings, use:

```objectivec
[[Kandy sharedInstance].services.group.settings
```
