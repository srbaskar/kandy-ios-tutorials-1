## Quick Start with Kandy iOS SDK

This guide covers what you need to go through in order to be able to start using Kandy SDK for iOS, in five steps:

1. Prerequisites
2. Download and Install Kandy SDK for iOS
3. Obtain a Kandy Domain Key and PIN
4. Configure your Xcode project
5. Start coding!