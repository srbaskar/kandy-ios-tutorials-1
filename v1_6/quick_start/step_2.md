### Step 2 - Download and Install Kandy SDK for iOS

Download the SDK zip file from:

<a href="http://gb-kandy-portal-shared.s3.amazonaws.com/downloads/iOSKandySDK_1.6.zip" class="btn btn-primary btn-sm">Download SDK</a>

Import the zip file contents into you existing iOS project, extracting it into your "frameworks" folder. If your project does not already have a frameworks folder, create one at the root of the project.

The zip contains:

* KandySDK.framework
* MobileSDK.framework
* KandySDK.docset
* Release notes

For Kandy SDK documentation:

In order to include the SDK manual, copy KandySDK.docset to the following path: ~/Library/Developer/Shared/Documentation/DocSets

To view the manual click the "Window->Documentation And API Reference" button on xCode