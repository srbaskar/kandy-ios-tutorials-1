### Step 4 - Configure your Xcode project

Add the Kandy SDK for iOS to your project and configure project file

#### Add the SDK to your app

In order to use the SDK, add the following frameworks to your project:

* KandySDK.framework
* MobileSDK.framework

You can do so by either dragging them to the Project Navigator, or by choosing "Add files" from the Project Navigator Context Menu.

#### Add required frameworks and libraries

In order for Kandy SDK to run, the following frameworks and libraries are required:

* CoreTelephony.framework
* GLKit.framework
* libstdc++.6.dylib
* libicucore.dylib
* CoreMedia.framework
* AudioToolbox.framework
* libsqlite3.dylib
* SystemConfiguration.framework
* AVFoundation.framework
* AddressBook.framework
* ImageIO.framework
* MobileCoreServices.framework
* Libc++.dylib
* CoreLocation.framework

#### Configure your app project file

##### Compilation

64-bit compilation is fully supported.

##### General Configuration

For the application to work as expected in the background, enter into the application's Info.plist the following configuration:

```markup
<key>UIBackgroundModes</key>
<array>
  <string>audio</string>
  <string>voip</string>
</array>
```
