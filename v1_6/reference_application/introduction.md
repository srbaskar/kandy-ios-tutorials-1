## Reference Application

Kandy has made a sample iOS application available to you which implements the basic services and functions outlined above.This application can be found in the Code section, [here](/code/ios-reference-application?version=ios_1_6).
