### Validation

#### Activation Code

This call sends the received code to the server for validation. On success, the system adds the user as a provisioned subscriber.

```objectivec
-(void)validate{
  NSString * validationCode = @"9876";
  KandyAreaCode * areaCode = [[KandyAreaCode alloc] initWithISOCode:@"US" andCountryName:@"United States" andPhonePrefix:@"+1"];
  NSString * strPhoneNumber = @"180000000000";
  [[Kandy sharedInstance].provisioning validateAndProvision:validationCode destination:strPhoneNumber areaCode:areaCode responseCallback:^(NSError *error, KandyUserInfo *provisionedUserInfo) {
    if (error) {
        // Failure
    } else {
        // Success
    }
  }];
}
```

#### Domain and User Provisioning Status

You can validate your domain and your user provisioning status using the method validateDomain:

```objectivec
-(IBAction)validateDomain:(id)sender{
[[Kandy sharedInstance].provisioning validateDomain:_domainTokenTextField.text domainSecret:_domainSecretTextField.text responseCallback:^(NSError *error, NSString *domainName) {
  if(error){
    //Fail
  }
  else{
    //Success
  }
}];
}
```

_**Note:** You can get the KandyUserInfo object of the user you subscribed from:_

* _The KandyUserInfo you received in the Validate method._
* _All provisioned users can be accessed using the following code:_

```objectivec
-[[Kandy sharedInstance].sessionManagement.provisionedUsers
```

#### Deactivation

This method deactivates a provisioned subscriber device:

```objectivec
-(void)deactivate{
  [[Kandy sharedInstance].provisioning deactivateWithResponseCallback:^(NSError *error) {
     if (error) {
          // Failure
      } else {
          // Success
      }
  }];
}
```
