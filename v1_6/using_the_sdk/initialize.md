### Initialize
Initialize the SDK by messaging the Kandy class and use one of the following methods:

* \<initializeSDKWithDomainKey:domainSecret:\>
  * Valid domain key and secret are used for making all SDK operations.
  * Passing nil for secret is used for all operations except provisioning.
  * Passing nil for both key and secret is only valid if you login with username@domain. It is used for all operations except provisioning.
* \<initializeSDKWithKandySession\> Used for passing previous registered KandySession

The preferred way of doing this is in the AppDelegate's application:didFinishLaunchingWithOptions.

```objectivec
-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [Kandy initializeSDKWithDomainKey:@"YourDomainKey" domainSecret:@"YourDomainSecret"];
  return YES;
}
```
