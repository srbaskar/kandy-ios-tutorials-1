## Log In

This method logs in a subscriber to the Kandy system using his user name and password (taken from the KandyUserInfo object):

```objectivec
-(void)login{
  KandyUserInfo * userInfo = [[KandyUserInfo alloc] initWithUserId:@"UserName" password:@"UserPassword"];
  [[Kandy sharedInstance].access login:userInfo responseCallback:^(NSError *error) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```

_**Note:** You can create KandyUserInfo either by the initializing it with user & password, or by taking the one you received from the Validate method.
After logging in to the system, you can also get the KandyUserInfo by the following code:_

```objectivec
[[Kandy sharedInstance].sessionManagement.currentUser
```
