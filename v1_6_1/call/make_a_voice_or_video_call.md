## Make a VoIP Call

The following method initiates a call, where:

* callee: the call destination
* isStartCallWithVideo: yes if you want a video call
* localView - the view that will present the self video
* remoteView - the view that will present the remote side video

  ```objectivec
  -(void) makeVoipCall {
    KandyRecord *callee = [[KandyRecord alloc] initWithURI:@"callee@domain"];
    KandyRecord *caller = [[KandyRecord alloc] initWithURI:@"caller@domain"];
    BOOL isStartCallWithVideo = YES;
    UIView *localView = <The preview you see the self video>;
    UIView *remoteView = <The preview you see the other side video>;

    id<KandyOutgoingCallProtocol> outgoingCall = [[Kandy sharedInstance].services.call createVoipCall:caller callee:callee options:callOptions];
    outgoingCall.localVideoView = localView;
    outgoingCall.remoteVideoView = remoteView;

    [outgoingCall establishWithResponseBlock:^(NSError *error) {
      if (error) {
        //Failure
      }
      else {
        //Success
      }
    }];
  }
  ```
