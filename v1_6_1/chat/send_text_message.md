## Send Text Message

To send an instant message, use the following code:

```objectivec
-(void)sendMsg {
  KandyRecord * kandyRecord = [[KandyRecord alloc] initWithURI:@"DestinationURI"];
  KandyChatMessage *textMessage = [[KandyChatMessage alloc] initWithText:@"Message text" recipient:kandyRecord];
  [[Kandy sharedInstance].services.chat sendChat:chatMessage progressCallback:^(KandyTransferProgress *transferProgress) {
    //progress
  }
  responseCallback:^(NSError *error) {
    if (error) {
      //Failure
    } else {
      //Success
    }
  }];
}
```
