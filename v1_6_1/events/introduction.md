# Events

Events service allows you to pull current pending events or events history from server. Events could be the chat messages, group chats and group changes events, or message delivery notifications.
