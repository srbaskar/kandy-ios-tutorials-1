## Pulling Events from History

To pull events from history, you can use relevant methods available in KandyEventService. Events pulled from history will be dispatched to Chat and Group service delegates, as it for pending events. All history events will have isHistoryEvent flag set to YES.

### Pull 20 latest events from history for specific destination: 

```

-(void)pullHistoryEventsForDestination:(KandyRecord*)destination
{
    [[Kandy sharedInstance].services.events pullHistoryEventsForDestination destination  timestamp:[NSDate date] moveForward:NO maxCount:20 responseCallback:^(NSError *  error, NSInteger numberOfEventsReceived, BOOL hasMoreEventsOnServer) {
        if(error)
        {
            //Failure
        }
        else
        {
            //Success
        }
    }];
}

```

### Pull 20 latest history events for all destinations for current provisioned user:

```
-(void)pullHistoryEventsForAllConversations 
{
    [[Kandy sharedInstance].services.events pullHistoryEventsWithTimestamp:[NSDate date] moveForward:NO maxCount:20 responseCallback:^(NSError * error, NSSet<KandyRecord *> * destinations, NSInteger numberOfEventsReceived, BOOL hasMoreEventsToPull) {
        if(error)
        {
            //Failure
        }
        else
        {
            //Success
        }
    }];
}

```
