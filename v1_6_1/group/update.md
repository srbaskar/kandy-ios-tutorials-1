## Group Update

You can update the group name and image. This method can also be used to remove the group image and name.

To remove a group name use: kRemoveGroupName

To remove a group image use: kRemoveGroupImage

```objectivec
-(void)createGroup{
  KandyRecord * groupId = self.kandyGroup.groupId;
  KandyGroupParams * params = [[KandyGroupParams alloc] init];
  kandyGroupParams.groupName = @"groupName";
  kandyGroupParams.groupAbsoluteImagePath = @"pathToGroupImage"; //will also initialize the group.image
  [[Kandy sharedInstance].services.group updateGroupParams:params groupId:groupId progressCallback:^(KandyTransferProgress *transferProgress) {
      NSLog(@"Update group image progress : %ld", (long)transferProgress.transferProgressPercentage);
  } responseCallback:^(NSError *error, KandyGroup *group) {
    if (error) {
      //error
    } else {
      //group created successfully
    }
  }];
}
```

_**Note:**_

* Update group name or group image only by calling: <br />updateGroupName, updateGroupImage
* Remove group image is also possible by calling: <br />removeGroupImage
