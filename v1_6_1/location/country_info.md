## Country Info

You can receive country codes based on your location (SIM and IP Address).

To receive a location-based country code use the following code:

```objectivec
-(void)getCountryInfo {
  [[Kandy sharedInstance].services.location getCountryInfoWithResponseCallback:^(NSError *error, KandyAreaCode *areaCode) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```
