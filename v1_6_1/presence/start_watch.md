## Start Watch Presence

Start watching other user’s presence state

```objectivec
startWatchPresenceForRecords:responseCallback:`
```

```objectivec

- (void)startWatch
 {
    KandyRecord* kandyRecord = [[KandyRecord alloc] initWithURI:user_id type:EKandyRecordType_contact];
   
 [[Kandy sharedInstance].services.presence startWatchPresenceForRecords:[NSArray arrayWithObject:kandyRecord] responseCallback:^(NSError *error) {
        UIAlertView *alert;
        
        if (error) {
		      //start watch failed
		  } else {
            //start watchn succeeded
        }
	}];
}

```
