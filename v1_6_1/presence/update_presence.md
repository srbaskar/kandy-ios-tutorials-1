### Update Presence

Presence state allows the user to update status, for ex: Away, at work etc...
To update the presence state call the method:


```objectivec
updateSelfPresenceState:presenceActivityType:withResponseCallback:
```

```objectivec

[[Kandy sharedInstance].services.presence updateSelfPresenceState:isOnline     
							      presenceActivityType:presenceStatusType
							      responseCallback:^(NSError *error) {

	if (error) {
		//update state failed
	}
 	else {
		//update state succeeded
	}
}];

```
