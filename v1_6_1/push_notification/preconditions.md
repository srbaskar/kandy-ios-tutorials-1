## Preconditions
Before you can utilize Kandy Notifications you must first complete the following steps (code examples provided are for iOS 8).

For more information, visit Apple web site: [Local and Remote Notification Programming Guide](https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Introduction.html)

* Create Push notification certificate via iOS developer portal. Please contact [Kandy Support](mailto:support@kandy.io) in order to publish the *.pem file. For more information see: [How to renew your Apple push notification push SSL certificate](https://blog.serverdensity.com/how-to-renew-your-apple-push-notification-push-ssl-certificate)

* In your AppDelegate, register your user notification settings using the following code:

  ```objectivec
  -(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //…
    //initialize the KandySDK
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        //Register UserNotificationSettings
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert) categories:nil]];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationType)(UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert)];
    }
  }
  ```

* Implement the user notification settings callback and register for remote notifications using the following code:

  ```objectivec
  -(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [[UIApplication sharedApplication] registerForRemoteNotifications];
  }
  ```

* You can store the received deviceToken for future use using the following code:

  ```objectivec
  -(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //…
    [[NSUserDefaults standardUserDefaults]setObject:deviceToken forKey:@"deviceToken"];
    NSString* bundleId = [[NSBundle mainBundle]bundleIdentifier];
    //…
  }
  ```
