This document includes the documentation for the Kandy iOS SDK
The code samples are shown in a developer-friendly window showing parsed code. The window also allows for easy copy-paste of the code by developers

# Quick Start with Kandy iOS SDK

This guide covers what you need to go through in order to be able to start using Kandy SDK for iOS, in five steps:

1. Prerequisites
2. Installing the Kandy SDK
3. Obtain a Kandy Domain KEY and SECRET
4. General Configuration
5. Start coding!
