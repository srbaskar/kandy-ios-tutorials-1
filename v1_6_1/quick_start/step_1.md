## Step 1 - Prerequisites

As you read through the documentation, let's ensure you understand the definition of a few terms used which also helps you understand how Kandy is organized.

First off, Kandy is a hierarchical system and the top most level is **ADMINISTRATOR.** Think of the Administrator as a super user who can effect any changes. Below the Administrator, is the **ACCOUNT**. Accounts may be set up for specific applications or for individual customers as well (if you're a developer working with multiple customers). You can name accounts according to your needs (for example customer\_1, customer\_2, etc).

From the Account Level moving down is **DOMAINS**. A domain could be used for an account which has multiple applications or perhaps to create a subsection of users. For example, you might create domains for a customer based upon regions (for example US, EMEA, APAC).

* OS X is required for all iOS development
* You need [Xcode](https://developer.apple.com/xcode/downloads/). If you don't have it, you can get it from [Apple](https://developer.apple.com/xcode/downloads/).


> **The Kandy SDK for iOS versions supports iOS7 and higher.**
