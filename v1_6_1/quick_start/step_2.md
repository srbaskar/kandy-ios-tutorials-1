## Step 2 - Installing the Kandy SDK

**`Manual Install`**

Download the SDK zip file from:

<a href="http://gb-kandy-portal-shared.s3.amazonaws.com/downloads/iOSKandySDK_1.6.8.zip" class="btn btn-primary btn-sm">Download SDK</a>

Import the zip file contents into you existing iOS project, extracting it into your "frameworks" folder. If your project does not already have a frameworks folder, create one at the root of the project.

The zip contains:

* KandySDK.framework
* KandySDK.docset
* SampleApp.zip

For Kandy SDK documentation:

In order to include the SDK manual, copy KandySDK.docset to the following path: ~/Library/Developer/Shared/Documentation/DocSets

To view the manual click the "Window->Documentation And API Reference" button on xCode

In order to use the SDK, add the following frameworks to your project:

* KandySDK.framework

You can do so by either dragging them to the Project Navigator, or by choosing "Add files" from the Project Navigator Context Menu.

###### Add required frameworks and libraries

In order for Kandy SDK to run, the following frameworks and libraries are required:

* VideoToolbox.framework
* CoreTelephony.framework
* GLKit.framework
* libstdc++.6.dylib
* libicucore.dylib
* CoreMedia.framework
* AudioToolbox.framework
* libsqlite3.dylib
* SystemConfiguration.framework
* AVFoundation.framework
* AddressBook.framework
* ImageIO.framework
* MobileCoreServices.framework
* Libc++.dylib
* CoreLocation.framework
* MediaPlayer.framework

**`Install the SDK using CocoaPods`**

[![Kandy Cocoapods](pod.png)](https://cocoapods.org/pods/Kandy)

You can install the CocoaPods tool on OS X by running the following command from the terminal. Detailed information is available in the [Getting Started guide](https://guides.cocoapods.org/using/getting-started.html#getting-started).

```ruby
$ sudo gem install cocoapods
```

**Create a Podfile for the Kandy SDK for iOS and use it to install the API and its dependencies:**

* Create a file named `Podfile` in your project directory. This file defines your project's dependencies, and is commonly referred   to as a Podspec.

```ruby
$ touch podfile
$ open -e podfile
```

* Edit the Podfile and add your dependencies. Here is a simple Podspec, including the name of the pod you need for the Kandy SDK for iOS:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '7.0'
pod "Kandy"
```

* Save the `Podfile`.
* Open a terminal and go to the directory containing the Podfile:

```ruby
$ cd <project-path>
```

* Run the pod `install` command. This will install the APIs specified in the Podspec, along with any dependencies they may have.

```ruby
$ pod install
```

* Close Xcode, and then open (double-click) your project's .xcworkspace file to launch Xcode. From this time onwards, you must use the .xcworkspace file to open the project.
