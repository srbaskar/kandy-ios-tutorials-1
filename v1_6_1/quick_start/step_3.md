## Step 3 - Obtain a Kandy Domain KEY and SECRET

If you haven't already registered with your app with Kandy, you should register with Kandy to start

Fill in the email:

![Sign Up](sign_up.png)

You will receive an email to validate your email address:

![Validate Email](validate_email.png)

Please click below to validate your account and get started embedding real-time communication in minutes! From your inbox:

![Setup Project](setup_project_1.png)

![Setup Project](setup_project_2.png)

Once you register with Kandy, Your next screen would be:

![Project Setup Success](project_setup_success.png)

Next you need to get DOMAIN KEY and DOMAIN SECRET and configure your Kandy application to support from iOS application, see below screen shot to obtain DOMAIN KEY and DOMAIN SECRET

![Project Dashboard](projects_dashboard.png)
