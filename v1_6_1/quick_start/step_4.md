## Step 4 - General Configuration

##### Compilation

64-bit compilation is fully supported.

##### Enable Background Mode

For the application to work as expected in the background, enter into the application's Info.plist the following configuration:

```markup
<key>UIBackgroundModes</key>
<array>
  <string>audio</string>
  <string>voip</string>
</array>
```
