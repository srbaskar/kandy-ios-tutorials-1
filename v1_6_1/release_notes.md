## Release Notes 

## v1.6.8 - February 20, 2016

### New Features:

* Access
  - Significantly reduced time needed to provisioned user to log in.
* Chat Service
  - Added new config parameter: -(BOOL)autoDownload_media_fromHistory.
* Event Service
  - KandyEventService: Events service allows you to pull current pending events or events history from server.
  - Added new APIs allowing to delete history events from server.
* Call Service
  - Reduced times needed to connect incoming call.
  - Added new parameters to KandyCallProtocol: -(float)remoteVideoAspectRatio and –(float)localVideoAspectRatio

### Fixes:

* General
  - Fixed stuck when doing end call
  - Fixed random crashes, greatly improved overall stability
  - Fixed various issues related to call handling
* Call Service
  - Fixed impossibility to make login with only user access token
  - Improved call quality and other general bug fixes related to call initiation and handling
* Presence Service
  - Performance improvement

### Breaking Changes:

* Call Service
 * SDK will not reject incoming calls automatically, in case and active GSM call detected or in any other cases. Now it is up to user to determine desired behavior.

* Chat Service
 - markAsReceived:responseCallback: method and pullEventsWithResponseCallback: now deprecated. Use relevant methods from KandyEventService instead.

v1.6.4

### New Features:

* Subscriber Provisioning
 - Added possibility to set the caller phone number prefix when requesting code by IVR. Phone number will be a combination of the provided prefix and the validation code.
* Chat Service
 - Added possibility to attach custom data to chat messages. See KandyMediaItemProtocol and docs (`Attaching custom data to message`).
* Presence
 - Presence State Updates
   * Enable user to update his presence status and listening to other users presence status updates
 - Get Last Seen
   * Method signature [retreivePresenceForRecords: responseCallback:]
    Changed [getLastSeenForRecords: responseCallback:]
 - Update Presence State
   * Method signature [updateSelfPresenceState:presenceActivityType: withResponseCallback:]
 - Start Watch Presence
   * Method signature [startWatchPresenceForRecords: withResponseCallback:]
 - Stop Watch Presence
   * Method signature [stopWatchPresenceForRecords: withResponseCallback:]

### Fixes:

* Call Service
 - Not established IP call (in ringing, dialing or answering state) now will be terminated if received native GSM call
 
### Breaking Changes:

* KandyCallProtocol
 - Property -caller renamed to -remoteRecord

v1.6.2

### New Features:
* KandySDK compiled with iOS9 SDK
* Subscriber Provisioning
  * Added support for sending OTP in different methods
  * Added support for IVR (Interactive voice response)  as a method to send activation code

* Contacts Service
  * Support for personal address book – enables you to create custom address book

* Call Service
  * New KandyCallServiceSettings
  * Support for transfer call (experimental)
  * Support for calls with blocked caller ID
  * Support for default camera position when initiating / getting a video call (configurable via KandyCallServiceSettings)
  * Support for disabling camera orientation  change in video calls (configurable via KandyCallServiceSettings)
  * New call state – “answering“ (EKandyCallState_answering)
  * KandySDK now able to reject incoming calls incase already in GSM call (configurable via KandyCallServiceSettings)
  * Support incoming PSTN calls – call type will be EKandyCallType_in
  * Already answered call – Now provided informative termination reason

* Push Notification  Service
  * Added support for VoIP Push Notifications (iOS 8.1 and above using PushKit)

* Chat Service
  * Support for messages with custom data
  * Added support for sending/receiving messages with file attachment

* Cloud Storage Service
  * New service added – you can now upload / download any kind of file

* Billing Service
  * New service added

### Fixes:

* Call Service
  * Fixed an issue when getting stuck on dialing state without option to end call

* Kandy Access
  * Multiple connectivity issues

### Breaking Changes:
  * Required frameworks and libraries
    * MobileSDK.framework is obsolete now and should be removed from your projects using Kandy SDK
    * Kandy SDK now depending on VideoToolbox.framework 

  * Access
    * Alongside with [[Kandy sharedInstance].access.connectionState where is a new state parameter: [[Kandy sharedInstance].access.registrationState
    

  * Call 
    * [KandyCallService createSIPTrunkCall:(KandyRecord*)caller callee:(KandyRecord*)callee] 
      syntax changed to:
      [KandyCallService createSIPTrunkCall:(NSString *)caller destination:(NSString *)internationalPhoneNumber]
    * Method changed - createVoipCall:callee:options: - options parameter takes new type EKandyOutgingVoIPCallOptions
    * Method - createPSTNCall:destination: - now takes new parameter options (EKandyOutgingPSTNCallOptions)

 * Address Book Service
    * updatePersonalConatct – added parameter contactID (NSString)
    * deletePersonalConatct – takes now contactID (NSString*) instead of params (KandyContactParams*)
     
 * Subscriber Provisioning
    * Method validateDomain:andDomainSecret:responseCallback: renamed to validateDomain:domainSecret:responseCallback:
    * Method requestCode:phoneNumber:responseCallback: now takes new parameter codeRetrivalMethod (EKandyValidationMethod)

  * Location Service
    * reportLocation: status: responseCallback: now takes locationParams (KandyLocationParams*) instead of location (CLLocation*)

  * Chat Service
    * Property removed from KandyChatService - messageBuilder (KandyMessageBuilder*)Use static methods from KandyMessageBuilder class

  * Contact Service
    * KandyContactsService method updatePersonalConatct:contactID:responseCallback: typo fixed updatePersonalContact:contactID: responseCallback:
    * KandyContactsService method deletePersonalContact:contactID:responseCallback: typo fixed deletePersonalContact:contactID: responseCallback:
    
  * Push Notification Service
    * [KandyPushNotificationService enableRemoteNotificationsWithToken:(NSData*)token bundleId:(NSString*)bundleId  isSandbox:(BOOL)isSandbox responseCallback:(void(^)(NSError* error))responseCallback] 
    
    added new parameter isVoipPush: 

    [KandyPushNotificationService  enableRemoteNotificationsWithToken:(NSData*)token bundleId:(NSString*)bundleId   isSandbox:(BOOL)isSandbox isVoipPush:(BOOL)isVoip responseCallback:(void(^)(NSError* error))responseCallback]
