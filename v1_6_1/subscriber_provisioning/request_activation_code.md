## Request Activation Code

This call will send a registration request to the server.

The verification method (currently supports SMS and IVR) sends a validation code to the user, which the user should enter on the "validate" method.

_**Note:** You can receive the `KandyAreaCode` by MCC/IP using the location service using `getCountryInfo` API call._

```objectivec
-(void)registerSubscriber{
  KandyAreaCode * areaCode = [[KandyAreaCode alloc] initWithISOCode:@"US" andCountryName:@"United States" andPhonePrefix:@"+1"];
  NSString * strPhoneNumber = @"<phone number>";
  [[Kandy sharedInstance].provisioning requestCode:areaCode phoneNumber:strPhoneNumber codeRetrivalMethod:EKandyValidationMethod_sms responseCallback:^(NSError *error, NSString *destinationToValidate) {
    if (error) {
      // Failure
    } else {
      // Success
    }
  }];
}
```

There is an additional option for IVR method that allows to send validation code as a part of caller’s phone number. You can specify prefix for an incoming call. Incoming call number will be a combination of the validation code followed by the prefix specified. 

```objectivec
-(void)registerSubscriber{
    KandyAreaCode * areaCode = [[KandyAreaCode alloc] initWithISOCode:@"US" andCountryName:@"United States" andPhonePrefix:@"+1"];
    NSString * strPhoneNumber = @"<phone number>";
    [[Kandy sharedInstance].provisioning requestCode:areaCode phoneNumber:strPhoneNumber codeRetrivalMethod:EKandyValidationMethod_call callerPhonePrefix:@"<caller phone prefix>" responseCallback:^(NSError *error, NSString *destinationToValidate) {
        if (error) {
            // Failure 
        } else {
            // Success
        }
	}];
}
```
